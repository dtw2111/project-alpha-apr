from django.shortcuts import render
from .models import Project
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

# Create your views here.


@login_required
def list_projects(request):
    project_list = Project.objects.filter(members=request.user)
    context = {
        "project_list": [],
    }
    for project in project_list:
        context["project_list"].append(
            {
                "name": project.name,
                "num_tasks": len(Task.objects.filter(project=project)),
                "project_object": project,
            }
        )

    return render(request, "projects/list.html", context)


@login_required
def show_project_detail(request, pk):
    project = Project.objects.get(pk=pk)
    tasks = Task.objects.filter(project=project).order_by("due_date")
    context = {
        "project": project,
        "tasks": tasks,
    }

    return render(request, "projects/detail.html", context)


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create.html"
    fields = ["name", "description", "members"]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])
