from django.urls import path

from .views import list_projects, show_project_detail, ProjectCreateView

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:pk>/", show_project_detail, name="show_project"),
    path("create/", ProjectCreateView.as_view(), name="create_project"),
]
